<?php

namespace Ubspack\UserCreateCommand;

use Ubspack\UserCreateCommand\Commands\UserCreateCommand;
use Illuminate\Support\ServiceProvider;

class UserCreateCommandProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                UserCreateCommand::class
            ]);
        }
    }

    public function register()
    {
        
    }
}